package org.example.DAO;

import org.example.entities.Booking;

import java.util.List;

public class BookingDAO extends DAO<Booking> {
    private static BookingDAO single_instance = null;
    public static synchronized BookingDAO getInstance() {
        if (single_instance == null) {
            single_instance = new BookingDAO();
        }
        return single_instance;
    }
    protected BookingDAO() {
        super("booking");
    }
}
