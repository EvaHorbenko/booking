package org.example.DAO;

import org.example.entities.Airline;
import org.example.entities.Airport;
import org.example.entities.Flight;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static org.example.utils.Randomizer.genRandomUnder;

public class FlightsDAO extends DAO<Flight> {

    private static FlightsDAO single_instance = null;
    public static synchronized FlightsDAO getInstance() {
        if (single_instance == null) {
            single_instance = new FlightsDAO();
        }
        return single_instance;
    }

    private FlightsDAO() {
        super("flights");
    }

    @Override
    public List<Flight> getAll() {
        List<Flight> list = super.getAll();
        if (list.isEmpty()) {
            genToList(list);
        }
        return list;
    }

    private void genToList(List<Flight> list) {
        for (int i = 0; i < 999; i++) {
            Airline airline = Airline.values()[genRandomUnder(Airline.values().length)];
            long time = Timestamp.valueOf(LocalDateTime.now()
                    .plusMinutes((long) genRandomUnder(60) * genRandomUnder(24)))
                    .getTime();
            Airport from = Airport.values()[genRandomUnder(Airport.values().length)];
            Airport to = Airport.values()[genRandomUnder(Airport.values().length)];
            int freeSeats = genRandomUnder(50);

            if (from != to) {
                list.add(new Flight(airline, time, from, to, freeSeats));
            }
        }

        try {
            set(list);
        } catch (Exception ignored) {}
    }
}
