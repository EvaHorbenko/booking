package org.example.DAO;

import org.example.interfaces.BusinessLogic;
import org.example.interfaces.IdentifiableSerializable;

import java.io.*;
import java.util.*;

public abstract class DAO<A extends IdentifiableSerializable> implements BusinessLogic<A> {

    private final File file;

    protected DAO(String fileName) {
        String dirName = "db";
        File dir = new File(dirName);
        if (!dir.exists()) {
            dir.mkdir();
        }
        String path = String.format("%s/%s.txt", dirName, fileName);
        this.file = new File(path);
    }

    @Override
    public void set(A a) throws Exception {
        ArrayList<A> as;
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            as = (ArrayList<A>) ois.readObject();
            ois.close();
        } catch (Exception x) {
            as = new ArrayList<>();
        }
        as.add(a);
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(as);
        oos.close();
}
    @Override
    public void set(List<A> list) throws Exception {
        ArrayList<A> as;
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            as = (ArrayList<A>) ois.readObject();
            ois.close();
        } catch (Exception x) {
            as = new ArrayList<>();
        }
        as.addAll(list);
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(as);
        oos.close();
    }

    @Override
    public List<A> getAll() {
        ArrayList<A> as;
        try{
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            as = (ArrayList<A>)ois.readObject();
            ois.close();
        } catch (Exception e) {
            as = new ArrayList<>();
        }
        return as;
    }
    @Override
    public A get(String code) throws Exception {
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        ArrayList<A> as = (ArrayList<A>)ois.readObject();
        ois.close();
        for (A a: as) {
            if (a.code().equals(code)) return a;
        }
        throw new IllegalStateException(String.format("given id:%d not found", code));
    }

    @Override
    public boolean remove(String code) throws Exception {
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        ArrayList<A> as = (ArrayList<A>)ois.readObject();
        ois.close();
        boolean isRemoved = as.removeIf(a -> a.code().equals(code));
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(as);
        oos.close();
        return isRemoved;
    }

    public void update(A a) throws Exception {
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        ArrayList<A> as = (ArrayList<A>)ois.readObject();
        ois.close();
        A old = as.stream()
                .filter(e -> e.code().equals(a.code()))
                .toList().get(0);
        int idx = as.indexOf(old);
        as.set(idx, a);
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(as);
        oos.close();
    }
}
