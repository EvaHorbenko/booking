package org.example.utils;

import java.util.Random;

public class Randomizer {
    public static int genRandomUnder(int max) {
        return new Random().nextInt(max);
    }

    public static int genRandomBetween(int min, int max) {
        return (new Random().nextInt(max - min) + min);
    }
}
