package org.example.entities;

import org.example.interfaces.IdentifiableSerializable;

import java.util.Objects;

public class Passenger implements IdentifiableSerializable {

    private final String code;
    private final String name;
    private final String surname;

    public Passenger(String name, String surname) {
        this.code = String.format("%s:%s", name.substring(0, 3), surname.substring(0, 3));
        this.name = name;
        this.surname = surname;
    }

    @Override
    public String code() {
        return code;
    }

    public String name() {
        return name;
    }

    public String surname() {
        return surname;
    }

    @Override
    public String toString() {
        return String.format("%s %s", name, surname);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passenger passenger = (Passenger) o;
        return Objects.equals(code, passenger.code) && Objects.equals(name, passenger.name) && Objects.equals(surname, passenger.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, surname);
    }
}
