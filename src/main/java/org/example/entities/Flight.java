package org.example.entities;

import org.example.interfaces.IdentifiableSerializable;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.example.utils.Randomizer.*;

public class Flight implements IdentifiableSerializable {
    private final String code;
    private final String airline;
    private final long dateTime;
    private final Airport from;
    private final Airport to;
    private int freeSeats;
    private final Set<Passenger> passengers = new HashSet<>();

    public Flight(Airline code, long dateTime, Airport from, Airport to, int freeSeats) {
        this.code = String.format("%s%s", code, genRandomBetween(100, 1000));
        this.airline = code.label;
        this.dateTime = dateTime;
        this.from = from;
        this.to = to;
        this.freeSeats = freeSeats;
    }
    public String code() {
        return code;
    }

    public Airport from() {
        return from;
    }

    public Airport to() {
        return to;
    }

    public LocalDate date() {
        return new Timestamp(dateTime).toLocalDateTime().toLocalDate();
    }

    public Set<Passenger> passengers() {
        return passengers;
    }

    public int freeSeats() {
        return freeSeats;
    }

    public boolean addPassenger(Passenger p) {
        if(!passengers.contains(p)) {
            passengers.add(p);
            freeSeats--;
            return true;
        }
        return false;
    }

    public boolean removePassenger(Passenger p) {
        if(passengers.contains(p)) {
            passengers.remove(p);
            freeSeats++;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String time = new Timestamp(dateTime)
                .toLocalDateTime()
                .format(DateTimeFormatter.ofPattern("dd/MM/yyyy, HH:mm"));
        return String.format(
                "%-5s | %10s - %-10s | %s | %3s | %-10s",
                code, from, to, time, freeSeats, airline);
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof Flight flight)) return false;
        return from == flight.from &&
                to == flight.to &&
                freeSeats == flight.freeSeats &&
                dateTime == flight.dateTime &&
                airline.equals(flight.airline) &&
                code.equals(flight.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, airline, dateTime, from, to, freeSeats);
    }
}
