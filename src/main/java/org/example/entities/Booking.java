package org.example.entities;

import org.example.interfaces.IdentifiableSerializable;

import java.util.Objects;

public class Booking implements IdentifiableSerializable {
    private final String code;
    private final String flightCode;
    private final Passenger passenger;

    public Booking(String flightCode, Passenger passenger) {
        this.code = String.format("%s-%s",
                passenger.code(), flightCode);
        this.flightCode = flightCode;
        this.passenger = passenger;
    }

    @Override
    public String code() {
        return code;
    }

    public String flightCode() {
        return flightCode;
    }

    public Passenger passenger() {
        return passenger;
    }

    @Override
    public String toString() {
        return String.format("%s | %s | %s", code, flightCode, passenger.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return Objects.equals(code, booking.code) && Objects.equals(flightCode, booking.flightCode) && Objects.equals(passenger, booking.passenger);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, flightCode, passenger);
    }
}
