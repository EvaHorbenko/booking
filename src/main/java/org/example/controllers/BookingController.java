package org.example.controllers;

import org.example.entities.Booking;
import org.example.entities.Flight;
import org.example.entities.Passenger;
import org.example.services.BookingService;
import org.example.services.FlightService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookingController {
    private static BookingController single_instance = null;
    public static synchronized BookingController getInstance() {
        if (single_instance == null) {
            single_instance = new BookingController();
        }
        return single_instance;
    }

    private final BookingService BS = BookingService.getInstance();
    private final FlightService FS = FlightService.getInstance();

    public void book(Flight f, Passenger p) {
        f.addPassenger(p);
        FS.update(f);
        BS.set(new Booking(f.code(), p));
    }

    public List<Booking> getBookingFor(Passenger p) {
        Optional<List<Booking>> oBooking = BS.get(p);
        return oBooking.orElseGet(ArrayList::new);
    }

    public boolean cancel(String code) {
        Optional<Booking> oBooking = BS.get(code);
        if (oBooking.isPresent()) {
            Booking b = oBooking.get();
            Optional<Flight> oFlight = FS.getFlight(b.flightCode());
            if (oFlight.isPresent()) {
                Flight f = oFlight.get();
                f.removePassenger(b.passenger());
                FS.update(f);
                return BS.remove(code);
            }
        }
        return false;
    }
}
