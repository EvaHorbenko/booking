package org.example.controllers;

import org.example.entities.Airport;
import org.example.entities.Flight;
import org.example.services.FlightService;

import java.util.List;
import java.util.Optional;

public class FlightController {

    private static FlightController single_instance = null;
    public static synchronized FlightController getInstance() {
        if (single_instance == null) {
            single_instance = new FlightController();
        }
        return single_instance;
    }

    private final FlightService FS = FlightService.getInstance();

    public List<Flight> getAllFlights() {
        return FS.getAllFlights();
    }

    public String getInfoAbout(String code) {
        return FS.getInfoAbout(code);
    }

    public List<Flight> findBy(Airport from, Airport to, String date, int quantity) {
        return FS.findBy(from, to, date, quantity);
    }

    public List<Flight> getMyFlights(String name, String surname) {
        return FS.getMyFlights(name, surname);
    }

    public Optional<Flight> getFlight(String code) {
        return FS.getFlight(code);
    }

    public Optional<Flight> getFlightFrom(String code, List<Flight> list) {
        return FS.getFlightFrom(code, list);
    }
}
