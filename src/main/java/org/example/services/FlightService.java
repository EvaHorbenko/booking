package org.example.services;

import org.example.DAO.FlightsDAO;
import org.example.entities.Airport;
import org.example.entities.Flight;
import org.example.entities.Passenger;
import org.example.table.Console;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FlightService {

    private static FlightService single_instance = null;
    public static synchronized FlightService getInstance() {
        if (single_instance == null) {
            single_instance = new FlightService();
        }
        return single_instance;
    }

    private final FlightsDAO FD = FlightsDAO.getInstance();

    public List<Flight> getAllFlights() {
        return FD.getAll();
    }

    public String getInfoAbout(String code) {
        try {
            return FD.get(code).toString();
        } catch (Exception e) {
            return String.format("%s", e);
        }
    }

    public List<Flight> findBy(Airport from, Airport to, String date, int quantity) {
        return FD.getAll().stream()
                .filter(f -> f.from() == from)
                .filter(f-> f.to() == to)
                .filter(f -> f.freeSeats() >= quantity)
                .filter(f -> LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy")).equals(f.date()))
                .collect(Collectors.toList());
    }

    public List<Flight> getMyFlights(String name, String surname) {
        return FD.getAll().stream()
                .filter(f -> f.passengers().stream()
                        .filter(p -> p.equals(new Passenger(name, surname)))
                        .toList()
                        .size() > 0)
                .toList();
    }

    public Optional<Flight> getFlight(String code) {
        try {
            return Optional.of(FD.get(code));
        } catch (Exception ignored) {
            return Optional.empty();
        }
    }

    public Optional<Flight> getFlightFrom(String code, List<Flight> list) {
        HashMap<String, Flight> map = new HashMap<>();
        list.forEach(f -> map.put(f.code(), f));
        return Optional.of(map.get(code));
    }

    public void update(Flight f) {
        try {
            FD.update(f);
        } catch (Exception e) {
            Console.println(String.format("Could not update flight: %s", f.toString()));
        }
    }
}
