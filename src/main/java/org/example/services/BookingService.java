package org.example.services;

import org.example.DAO.BookingDAO;
import org.example.entities.Booking;
import org.example.entities.Passenger;

import java.util.List;
import java.util.Optional;

public class BookingService {
    private static BookingService single_instance = null;
    public static synchronized BookingService getInstance() {
        if (single_instance == null) {
            single_instance = new BookingService();
        }
        return single_instance;
    }

    private final BookingDAO BD = BookingDAO.getInstance();
    public void set(Booking b) {
        try {
            BD.set(b);
        } catch (Exception ignored) {}
    }

    public void set(List<Booking> list) {
        try {
            BD.set(list);
        } catch (Exception ignored) {}
    }

    public List<Booking> getAll() {
        return BD.getAll();
    }

    public Optional<Booking> get(String code) {
         try {
             return Optional.of(BD.get(code));
         } catch (Exception ignored) {
             return Optional.empty();
         }
    }

    public Optional<List<Booking>> get(Passenger p) {
        try {
            return Optional.of(
                    BD.getAll().stream()
                            .filter(b -> b.passenger().equals(p))
                            .toList());
        } catch (Exception ignored) {
            return Optional.empty();
        }
    }

    public boolean remove(String code) {
        try {
            return BD.remove(code);
        } catch (Exception ignored) {
            return false;
        }
    }
}
