package org.example;

import org.example.table.App;

public class Main {
    public static void main(String[] args) {
        App app = App.getInstance();
        app.run();
    }
}