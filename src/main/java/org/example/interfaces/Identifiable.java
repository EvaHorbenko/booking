package org.example.interfaces;

public interface Identifiable<A> {
    String code();
}
