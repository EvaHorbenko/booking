package org.example.interfaces;

import java.io.Serializable;

public interface IdentifiableSerializable extends Identifiable, Serializable {}
