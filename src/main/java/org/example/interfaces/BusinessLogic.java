package org.example.interfaces;

import java.util.List;

public interface BusinessLogic<T extends IdentifiableSerializable> {
    List<T> getAll() throws Exception;

    T get(String code) throws Exception;

    void set(T t) throws Exception;

    void set(List<T> list) throws Exception;

    default void remove(T t) throws Exception {
        remove(t.code());
    }

    boolean remove(String code) throws Exception;

    void update(T t) throws Exception;
}
