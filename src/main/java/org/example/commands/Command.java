package org.example.commands;

public interface Command {
    void apply();
    default boolean isExit() {
        return false;
    };
}
