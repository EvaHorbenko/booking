package org.example.commands;

import org.example.controllers.BookingController;
import org.example.entities.Booking;
import org.example.entities.Passenger;
import org.example.table.Console;

import java.util.List;

public class MyFlights implements Command {
    @Override
    public void apply() {
        String name = Console.askFor("Введите имя");
        String surname = Console.askFor("Введите фамилию");
        List<Booking> list = BookingController.getInstance().getBookingFor(new Passenger(name, surname));
        if (list.isEmpty()) {
            Console.println(String.format("У \"%s %s\" пока нет рейсов.", name, surname));
        } else {
            list.forEach(f -> Console.println(f.toString()));
        }
    }
}
