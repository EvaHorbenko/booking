package org.example.commands;

import org.example.table.Console;

public class UnexpectedCommand implements Command {
    @Override
    public void apply() {
        Console.println("Вы ввели несуществующею команду.");
    }
}
