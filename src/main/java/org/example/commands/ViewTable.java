package org.example.commands;

import org.example.controllers.FlightController;
import org.example.table.Console;

public class ViewTable implements Command {
    @Override
    public void apply() {
        FlightController.getInstance()
                .getAllFlights()
                .forEach(f -> Console.println(f.toString()));
    }
}
