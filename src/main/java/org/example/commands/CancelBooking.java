package org.example.commands;

import org.example.controllers.BookingController;
import org.example.table.Console;

public class CancelBooking implements Command {
    @Override
    public void apply() {
        String code = Console.askFor("Введите код бронирования, которое хотите отменить");
        boolean isRemoved = BookingController.getInstance().cancel(code);
        if (isRemoved) {
            Console.println(String.format("Бронирование %s было успешно удалено.", code));
        } else {
            Console.println(String.format("Брониование %s не найдено.", code));
        }
    }
}
