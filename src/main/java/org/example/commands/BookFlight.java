package org.example.commands;

import org.example.controllers.BookingController;
import org.example.controllers.FlightController;
import org.example.entities.Airport;
import org.example.entities.Flight;
import org.example.entities.Passenger;
import org.example.table.Console;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

public class BookFlight implements Command {
    @Override
    public void apply() {
        Airport from = from();
        Airport to = to();
        String date = date();
        int qty = quantity();

        findBy(from, to, date, qty);
    }

    private Airport from() {
        while (true) {
            try {
                return Airport.valueOf(Console.askFor("Введите место вылета"));
            } catch (Exception ignored) {
                Console.println("Вы ввели несуществующий аеропорт.");
            }
        }
    }

    private Airport to() {
        while (true) {
            try {
                return Airport.valueOf(Console.askFor("Введите место назначения"));
            } catch (Exception ignored) {
                Console.println("Вы ввели несуществующий аеропорт.");
            }
        }
    }

    private String date() {
        while (true) {
            try {
                String date = Console.askFor("Введите желаемую дату");
                LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                return date;
            } catch (Exception ignored) {
                Console.println("Введенное значение не являеться корректной датой.");
            }
        }
    }

    private int quantity() {
        while (true) {
            try {
                return Integer.parseInt(Console.askFor("Сколько билетов вам необходимо"));
            } catch (Exception ignored) {
                Console.println("Введенное значение небыло числом.");
            }
        }
    }

    private void findBy(Airport from, Airport to, String date, int qty) {
        List<Flight> list = FlightController.getInstance().findBy(from, to, date, qty);
        if (list.isEmpty()) {
            Console.println("Нет рейсов удовлетворяющих требования.");
        } else {
            Console.println("Рейсы удовлетворяющие требования: ");
            list.forEach(f -> Console.println(f.toString()));
            chooseFrom(list, qty);
        }
    }

    private void chooseFrom(List<Flight> list, int qty) {
        while (true) {
            try {
                Optional<Flight> oFlight = FlightController.getInstance()
                        .getFlightFrom(Console.askFor("Выберите рейс"), list);
                if (oFlight.isPresent()) {
                    Flight f = oFlight.get();
                    for (int i = 0; i < qty; i++) {
                        bookFlight(f, aksPassenger());
                    }
                    break;
                }
            } catch (Exception ignored) {
                Console.println("Выбранного рейса нет в списке");
            }
        }
    }

    private Passenger aksPassenger() {
        return new Passenger(
                Console.askFor("Введите имя пассажира"),
                Console.askFor("Введите фамилию пассажира"));
    }

    private void bookFlight(Flight f, Passenger p) {
        BookingController.getInstance().book(f, p);
    }
}
