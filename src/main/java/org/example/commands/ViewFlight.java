package org.example.commands;

import org.example.controllers.FlightController;
import org.example.table.Console;

public class ViewFlight implements Command {
    @Override
    public void apply() {
        Console.println(FlightController.getInstance().getInfoAbout(Console.askFor("Введите код рейса")));
    }
}
