package org.example.table;

import org.example.commands.*;

public class App {
    private static App single_instance = null;

    public static synchronized App getInstance() {
        if (single_instance == null) {
            single_instance = new App();
        }
        return single_instance;
    }

    public void run() {
        Menu menu = new Menu();
        menu.add("Посмореть все рейсы", new ViewTable());
        menu.add("Посмотреть информацию о рейсе", new ViewFlight());
        menu.add("Поиск и бронировка рейса", new BookFlight());
        menu.add("Отменить бронирование", new CancelBooking());
        menu.add("Мои рейсы", new MyFlights());
        menu.add("Выход", new Exit());

        while (true) {
            Command cmd = menu.listen();
            if (!cmd.isExit()) {
                cmd.apply();
            } else {
                break;
            }
        }
    }
}
