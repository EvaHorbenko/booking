package org.example.table;

import org.example.entities.Flight;

import java.util.Scanner;

public class Console {

    private static Scanner scanner = new Scanner(System.in);

    public static void print(String str) {
        System.out.print(str);
    }

    public static String readLine() {
        return scanner.nextLine();
    }

    public static void println(String str) {
        System.out.println(str);
    }

    public static String askFor(String str) {
        System.out.printf("%s: ", str);
        return scanner.nextLine();
    }
}
